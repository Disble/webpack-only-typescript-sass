export default class Person {
    private _name: string;
    private _lastname: string;
    constructor(name: string, lastname: string) {
        this._name = name;
        this._lastname = lastname;
    }
    public get name(): string {
        return this._name;
    }
    public set name(value: string) {
        this._name = value;
    }
    public get lastname(): string {
        return this._lastname;
    }
    public set lastname(value: string) {
        this._lastname = value;
    }
}