import Person from './Person';
import '../sass/styles.scss';

let person = new Person('Jeane', 'D\'Arc');
console.log(`${person.name} ${person.lastname}`);
console.log('Person', person);

let el = document.createElement('h1');
el.innerText = 'Webpack, Typescript y Sass te saludan!';

document.body.appendChild(el);

